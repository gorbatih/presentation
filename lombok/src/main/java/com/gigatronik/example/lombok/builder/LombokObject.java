package com.gigatronik.example.lombok.builder;

import java.util.Map;

import lombok.*;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
@Builder
@Getter
@ToString
public class LombokObject {
    @NonNull
    private String method;
    @Singular("header")
    private Map<String, String> headerMap;
    private String body;
}
