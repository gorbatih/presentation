package com.gigatronik.example.eventbus.ui.eventbus;

import java.awt.*;

import com.gigatronik.example.eventbus.ui.event.BaseEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class BackgroundToUIRepeater {
    private final EventBus uiEventBus;

    public BackgroundToUIRepeater(EventBus uiEventBus) {
        this.uiEventBus = uiEventBus;
    }

    @Subscribe
    public void onEvent(BaseEvent event) {
        if (event.repeat()) {
            String thread = Thread.currentThread().getName();
            System.out.println("[" + thread + "] repeat " + event);

            EventQueue.invokeLater(() -> uiEventBus.post(event));
        }
    }
}
