package com.gigatronik.example.lombok.entity;

import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class LombokEntityTest {
    @Test(expected = NullPointerException.class)
    public void testConstructor_nullValue_NPE() {
        new LombokEntity(1, null, new Date());
    }

    @Test(expected = NullPointerException.class)
    public void testSetter_nullValue_NPE() {
        LombokEntity testling = new LombokEntity();

        testling.setName(null);
    }
}