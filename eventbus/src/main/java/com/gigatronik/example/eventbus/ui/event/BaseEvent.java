package com.gigatronik.example.eventbus.ui.event;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class BaseEvent {
    private boolean repeated;

    public boolean repeat() {
        if (repeated) {
            return false;
        }
        repeated = true;
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{}";
    }
}
