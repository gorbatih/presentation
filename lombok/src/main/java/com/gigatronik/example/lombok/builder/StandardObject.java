package com.gigatronik.example.lombok.builder;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class StandardObject {
    private String method;
    private Map<String, String> header;
    private String body;

    protected StandardObject(String method, Map<String, String> header, String body) {
        this.method = method;
        this.header = header;
        this.body = body;
    }

    public String getMethod() {
        return method;
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public String getBody() {
        return body;
    }

    public static class Builder {
        private String method;
        private Map<String, String> header = new HashMap<String, String>();
        private String body;

        public Builder setMethod(String method) {
            this.method = method;
            return this;
        }

        public Builder addHeader(String key, String value) {
            header.put(key, value);
            return this;
        }

        public Builder setBody(String body) {
            this.body = body;
            return this;
        }

        public StandardObject build() {
            if (method == null) {
                throw new NullPointerException("method");
            }
            return new StandardObject(method, header, body);
        }
    }
}
