package com.gigatronik.example.eventbus.ui;

import com.gigatronik.example.eventbus.ui.event.RequestEvent;
import com.gigatronik.example.eventbus.ui.event.ResultEvent;
import com.gigatronik.example.eventbus.ui.event.StartEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class UIComponent {
    private final EventBus eventBus;

    public UIComponent(EventBus eventBus) {
        this.eventBus = eventBus;

        eventBus.register(this);
    }

    @Subscribe
    public void onStartup(StartEvent event) {
        RequestEvent requestEvent = new RequestEvent(19, 23);

        String thread = Thread.currentThread().getName();
        System.out.println("[" + thread + "] receive " + event + ", send " + requestEvent);

        eventBus.post(requestEvent);
    }

    @Subscribe
    public void onResult(ResultEvent event) {
        String thread = Thread.currentThread().getName();
        System.out.println("[" + thread + "] receive " + event);
    }
}
