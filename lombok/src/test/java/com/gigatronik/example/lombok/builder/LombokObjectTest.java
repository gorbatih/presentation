package com.gigatronik.example.lombok.builder;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class LombokObjectTest {
    @Test(expected = NullPointerException.class)
    public void testBuilder_methodIsNull_NPE() {
        LombokObject.builder().build();
    }

    @Test
    public void testBuilder_methodIsNotNull_success() {
        LombokObject object = LombokObject.builder()
                                          .method("Test")
                                          .build();

        assertEquals("Test", object.getMethod());
        assertNotNull(object.getHeaderMap());
    }

    @Test
    public void testBuilder_addHeader_success() {
        LombokObject object = LombokObject.builder()
                                          .method("Test")
                                          .header("key", "value")
                                          .build();

        assertEquals("Test", object.getMethod());
        assertEquals(1, object.getHeaderMap().size());
        assertTrue(object.getHeaderMap().containsKey("key"));
        assertEquals("value", object.getHeaderMap().get("key"));
    }
}