package com.gigatronik.example.lombok.entity;

import java.util.Date;

import lombok.*;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class LombokEntity {
    private int id;
    @NonNull
    private String name;
    private Date creationDate;
}
