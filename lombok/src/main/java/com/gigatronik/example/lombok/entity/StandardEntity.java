package com.gigatronik.example.lombok.entity;

import java.util.Date;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class StandardEntity {
    private int id;
    private String name;
    private Date creationDate;

    public StandardEntity() {
    }

    public StandardEntity(int id, String name, Date creationDate) {
        this.id = id;
        this.name = name;
        this.creationDate = creationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StandardEntity standardEntity = (StandardEntity) o;

        if (id != standardEntity.id) {
            return false;
        }
        if (!name.equals(standardEntity.name)) {
            return false;
        }
        return creationDate.equals(standardEntity.creationDate);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + creationDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "StandardEntity{" +
               "id=" + id +
               ", name='" + name + '\'' +
               ", creationDate=" + creationDate +
               '}';
    }
}
