package com.gigatronik.example.lombok.builder;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class BuildersTest {
    @Test
    public void testGetBuilder_noBody_success() {
        SimpleObject object = Builders.get()
                                      .build();

        assertEquals("GET", object.getMethod());
    }

    @Test(expected = NullPointerException.class)
    public void testPostBuilder_noBody_NPE() {
        Builders.post().build();
    }

    @Test
    public void testPostBuilder_body_success() {
        SimpleObject object = Builders.post()
                                      .body("test")
                                      .build();

        assertEquals("POST", object.getMethod());
    }
}