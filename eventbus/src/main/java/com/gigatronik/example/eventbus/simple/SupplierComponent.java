package com.gigatronik.example.eventbus.simple;

import com.google.common.eventbus.EventBus;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class SupplierComponent {
    private final EventBus eventBus;

    private int counter = 0;

    public SupplierComponent(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public void someMethod() {
        SomeEvent event = new SomeEvent("test " + (counter++));

        String thread = Thread.currentThread().getName();
        System.out.println("[" + thread + "] send " + event);

        eventBus.post(event);
    }
}
