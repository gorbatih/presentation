package com.gigatronik.example.lombok.builder;

import java.util.Map;

import lombok.*;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
@Value
public class SimpleObject {
    @NonNull
    private String method;
    private Map<String, String> headerMap;
    private String body;
}
