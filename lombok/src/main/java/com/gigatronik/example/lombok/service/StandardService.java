package com.gigatronik.example.lombok.service;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class StandardService {
    private static final Logger LOGGER = LoggerFactory.getLogger(StandardService.class);

    private final InputStream is;

    public StandardService(InputStream is) {
        this.is = is;
    }

    public void readNext() throws IOException {
        LOGGER.info("next={}", is.read());
    }
}
