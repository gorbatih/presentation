package com.gigatronik.example.lombok.service;

import java.io.IOException;
import java.io.InputStream;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
@RequiredArgsConstructor
@Slf4j
public class LombokService {
    @NonNull
    private final InputStream is;

    public void readNext() throws IOException {
        log.info("next={}", is.read());
    }
}
