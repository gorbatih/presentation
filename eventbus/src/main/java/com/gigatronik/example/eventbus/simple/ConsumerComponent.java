package com.gigatronik.example.eventbus.simple;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class ConsumerComponent {
    public ConsumerComponent(EventBus eventBus) {
        eventBus.register(this);
    }

    @Subscribe
    public void onSomeEvent(SomeEvent event) {
        String thread = Thread.currentThread().getName();
        System.out.println("[" + thread + "] receive " + event);
    }
}
