package com.gigatronik.example.eventbus.ui.event;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class ResultEvent extends BaseEvent {
    private final int result;

    public ResultEvent(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "ResultEvent{" +
               "result=" + result +
               '}';
    }
}
