package com.gigatronik.example.lombok.builder;

import java.util.Map;

import lombok.*;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class Builders {
    @Builder(builderMethodName = "get", builderClassName = "SimpleObjectGetBuilder")
    public static SimpleObject simpleObjectGetBuilder(@Singular("header") Map<String, String> headerMap,
                                                      String body) {
        return new SimpleObject("GET", headerMap, body);
    }

    @Builder(builderMethodName = "post", builderClassName = "SimpleObjectPostBuilder")
    public static SimpleObject simpleObjectPostBuilder(@Singular("header") Map<String, String> headerMap,
                                                       @NonNull String body) {
        return new SimpleObject("POST", headerMap, body);
    }
}
