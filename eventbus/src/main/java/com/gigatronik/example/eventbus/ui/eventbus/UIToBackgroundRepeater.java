package com.gigatronik.example.eventbus.ui.eventbus;

import com.gigatronik.example.eventbus.ui.event.BaseEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class UIToBackgroundRepeater {
    private final EventBus backgroundEventBus;

    public UIToBackgroundRepeater(EventBus backgroundEventBus) {
        this.backgroundEventBus = backgroundEventBus;
    }

    @Subscribe
    public void onEvent(BaseEvent event) {
        if (event.repeat()) {
            String thread = Thread.currentThread().getName();
            System.out.println("[" + thread + "] repeat " + event);

            backgroundEventBus.post(event);
        }
    }
}
