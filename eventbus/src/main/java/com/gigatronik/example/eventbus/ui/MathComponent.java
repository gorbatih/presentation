package com.gigatronik.example.eventbus.ui;

import com.gigatronik.example.eventbus.ui.event.RequestEvent;
import com.gigatronik.example.eventbus.ui.event.ResultEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class MathComponent {
    private final EventBus eventBus;

    public MathComponent(EventBus eventBus) {
        this.eventBus = eventBus;

        eventBus.register(this);
    }

    @Subscribe
    public void onRequest(RequestEvent event) {
        ResultEvent resultEvent = new ResultEvent(event.getA() + event.getB());

        String thread = Thread.currentThread().getName();
        System.out.println("[" + thread + "] receive " + event + ", send " + resultEvent);

        eventBus.post(resultEvent);
    }
}
