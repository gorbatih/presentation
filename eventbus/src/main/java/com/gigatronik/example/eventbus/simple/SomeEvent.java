package com.gigatronik.example.eventbus.simple;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class SomeEvent {
    private final String string;

    public SomeEvent(String string) {
        this.string = string;
    }

    public String getValue() {
        return string;
    }

    @Override
    public String toString() {
        return "RequestEvent{" +
               "string='" + string + '\'' +
               '}';
    }
}
