package com.gigatronik.example.eventbus.simple;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class Main {
    private static void runApplication(EventBus eventBus) {
        SupplierComponent supplierComponent = new SupplierComponent(eventBus);
        ConsumerComponent consumerComponent = new ConsumerComponent(eventBus);

        supplierComponent.someMethod();
        supplierComponent.someMethod();
        supplierComponent.someMethod();
    }

    public static void main(String[] args) {
        {
            System.out.println("--- Sync ----");
            runApplication(new EventBus());
        }

        {
            ExecutorService executor = Executors.newFixedThreadPool(2);

            System.out.println("--- Async ----");
            runApplication(new AsyncEventBus(executor));

            executor.shutdown();
        }
    }
}
