package com.gigatronik.example.eventbus.ui;

import static java.lang.Thread.sleep;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.gigatronik.example.eventbus.ui.event.StartEvent;
import com.gigatronik.example.eventbus.ui.eventbus.BackgroundToUIRepeater;
import com.gigatronik.example.eventbus.ui.eventbus.UIToBackgroundRepeater;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class Main {
    private final ExecutorService executor;
    private final EventBus uiEventBus;
    private final EventBus backgroundEventBus;

    public Main() {
        executor = Executors.newFixedThreadPool(2);

        uiEventBus = new EventBus("ui");
        backgroundEventBus = new AsyncEventBus("background", executor);
        uiEventBus.register(new UIToBackgroundRepeater(backgroundEventBus));
        backgroundEventBus.register(new BackgroundToUIRepeater(uiEventBus));

        UIComponent uiComponent = new UIComponent(uiEventBus);
        MathComponent mathComponent = new MathComponent(backgroundEventBus);
    }

    private void startApplication() {
        backgroundEventBus.post(new StartEvent());
    }

    private void closeApplication() {
        executor.shutdown();
    }

    public static void main(String[] args) throws Exception {
        Main main = new Main();
        main.startApplication();
        sleep(1000);
        main.closeApplication();
    }
}
