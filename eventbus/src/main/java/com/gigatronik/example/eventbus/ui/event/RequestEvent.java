package com.gigatronik.example.eventbus.ui.event;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
public class RequestEvent extends BaseEvent {
    private final int a;
    private final int b;

    public RequestEvent(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    @Override
    public String toString() {
        return "RequestEvent{" +
               "a=" + a +
               ", b=" + b +
               '}';
    }
}
