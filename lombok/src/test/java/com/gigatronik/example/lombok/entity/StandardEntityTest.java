package com.gigatronik.example.lombok.entity;


import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;

/**
 * @author Vyacheslav Gorbatykh
 * @since 26.08.2018
 */
@Ignore
public class StandardEntityTest {
    @Test(expected = NullPointerException.class)
    public void testConstructor_nullValue_NPE() {
        new StandardEntity(1, null, new Date());
    }

    @Test(expected = NullPointerException.class)
    public void testSetter_nullValue_NPE() {
        StandardEntity testling = new StandardEntity();

        testling.setName(null);
    }
}